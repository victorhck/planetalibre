#!/bin/bash

# Editar las siguientes líneas con la información requerida

# Ruta a los archivos del planet.py
PLANET_PATH="/home/pi/planeta/"
# Ruta al repositorio local de Github con la web. Por seguridad debe ser diferente al del planet
LOCAL_REPO="/home/pi/planetalibre/public/"
# Usuario de Github
USERNAME="victorhck"
# Contraseña ¡¡¡Se almacena en texto plano!!!
PASSWORD="fake_contraseña:P" #NO es mi Contraseña!!
# URL al repositorio de Github
REMOTE_REPO="gitlab.com/victorhck/planetalibre"

######################################################
######################################################
date_var="$(date)"
# Se accede al directorio del planet y se actualizan las entradas
cd $PLANET_PATH
/usr/bin/python3 planet.py --no-browser
# Se copian los archivos html al repositorio local
cd salida 
/bin/cp *.html *.xml $LOCAL_REPO
cd $LOCAL_REPO
/usr/bin/git pull
# Se suben los cambios a Github
# cd $LOCAL_REPO
/usr/bin/git add .
/usr/bin/git commit -a -m "Actualización $date_var"
/usr/bin/git push -u https://$USERNAME:$PASSWORD@$REMOTE_REPO master

# /usr/local/bin/toot post -m /home/pi/planetalibre/public/imagenes/dots_connected.png "Actualizados los feeds en #PlanetaLibre. Echa un vistazo a las novedades en este enlace:  https://victorhck.gitlab.io/planetalibre/ "
